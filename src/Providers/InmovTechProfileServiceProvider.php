<?php

namespace Inmovsoftware\ProfileApi\Providers;

use Illuminate\Support\ServiceProvider;
use Inmovsoftware\ProfileApi\Http\Resources\V1\GlobalCollection;
use Illuminate\Support\Facades\Artisan;

class InmovTechProfileServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');

        $this->publishes([
            __DIR__.'/../../resources/lang' => resource_path('/lang'),
                ], 'profileLangs');

        Artisan::call('vendor:publish' , [
                      '--tag' => 'profileLangs',
                    '--force' => true,
        ]);

        $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'profileLangs');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->make('Inmovsoftware\ProfileApi\Models\V1\Profile');
        $this->app->make('Inmovsoftware\ProfileApi\Http\Controllers\V1\ProfileController');
        $this->registerHandler();
    }


    protected function registerHandler()
    {
        \App::singleton(
            Illuminate\Http\Resources\Json\ResourceCollection::class,
            GlobalCollection::class
        );

    }


}
