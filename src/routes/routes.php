<?php
use Illuminate\Http\Request;

Route::middleware(['api', 'jwt' ])->group(function () {
Route::group([
    'prefix' => 'api/v1'
], function () {
    Route::apiResource('profile', 'Inmovsoftware\ProfileApi\Http\Controllers\V1\ProfileController');
    Route::get('profile/list/all', 'Inmovsoftware\ProfileApi\Http\Controllers\V1\ProfileController@index_select');
        });
});
